import 'package:word_jar/models/player.dart';

class Jar {
  final String name;
  final Iterable<Player> players;

  const Jar(this.name, this.players);
}
