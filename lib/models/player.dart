import 'dart:collection';

class Player {
  String name;
  int score;

  Player(this.name, this.score);

  Player.fromMap(LinkedHashMap<Object?, Object?> data)
      : this.name = data['name'] as String,
        this.score = data['score'] as int;

  LinkedHashMap<Object?, Object?> toLinkedHashMap() => LinkedHashMap.from({
        'name': name,
        'score': score,
      });
}
