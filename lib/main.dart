import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'word_jar.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final FirebaseApp app = await Firebase.initializeApp();

  runApp(
    WordJar(app: app),
  );
}
