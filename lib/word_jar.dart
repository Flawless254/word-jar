import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:word_jar/views/jar_view.dart';

class WordJar extends StatelessWidget {
  final FirebaseApp app;

  const WordJar({Key? key, required this.app}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Word Jar',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: JarView(
        app: app,
      ),
    );
  }
}
