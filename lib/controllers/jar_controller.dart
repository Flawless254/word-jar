import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class JarController {
  final FirebaseDatabase database;
  DatabaseReference? _jarRef;
  DatabaseReference? _players;

  JarController(FirebaseApp app)
      : this.database = FirebaseDatabase(
          app: app,
          databaseURL:
              'https://word-jar-20637-default-rtdb.europe-west1.firebasedatabase.app',
        ) {
    database.setPersistenceEnabled(true);
  }

  DatabaseReference get jarRef {
    assert(_jarRef != null, 'Open the jar first, dummy');

    return _jarRef!;
  }

  DatabaseReference get players {
    assert(_players != null, 'Open the jar first, dummy');

    return _players!;
  }

  void openJar(String name) {
    _jarRef = database.reference().child('jars/tipo');
    _jarRef!.keepSynced(true);
    _players = _jarRef!.reference().child('players');
    _players!.keepSynced(true);
  }
}
