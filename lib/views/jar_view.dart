import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:word_jar/controllers/jar_controller.dart';
import 'package:word_jar/models/player.dart';
import 'package:word_jar/widgets/sum_billboard.dart';
import 'package:word_jar/widgets/trailing_counter.dart';

class JarView extends StatefulWidget {
  static const routeName = 'jar';
  final jarName = 'Tipo';
  final FirebaseApp app;

  JarView({
    Key? key,
    required this.app,
  }) : super(key: key);

  @override
  State<JarView> createState() => _JarViewState();
}

class _JarViewState extends State<JarView> {
  late final _jarController = JarController(widget.app);

  @override
  void initState() {
    super.initState();
    _jarController.openJar(widget.jarName.toLowerCase());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.jarName),
      ),
      body: Column(
        children: [
          const Spacer(flex: 1),
          Expanded(
            flex: 5,
            child: SumBillboard(
              jarController: _jarController,
            ),
          ),
          Expanded(
            flex: 5,
            child: FirebaseAnimatedList(
              query: _jarController.players,
              itemBuilder: (context, snapshot, animation, index) {
                final player = Player.fromMap(snapshot.value);

                return SizeTransition(
                  sizeFactor: animation,
                  child: ListTile(
                    title: Text(
                      '${player.name}',
                    ),
                    trailing: TrailingCounter(
                      onRemove: player.score <= 0
                          ? null
                          : () {
                              player.score--;
                              _jarController.players
                                  .child(snapshot.key!)
                                  .set(player.toLinkedHashMap());
                            },
                      count: player.score,
                      onAdd: () async {
                        player.score++;
                        await _jarController.players
                            .child(snapshot.key!)
                            .set(player.toLinkedHashMap());
                      },
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
