import 'dart:ffi';

import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:word_jar/controllers/jar_controller.dart';

class SumBillboard extends StatelessWidget {
  final JarController jarController;

  const SumBillboard({
    Key? key,
    required this.jarController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FirebaseAnimatedList(
      query: jarController.jarRef,
      itemBuilder: (context, snapshot, animation, index) {
        if (snapshot.value is List) {
          int totalScore = 0;

          snapshot.value.forEach((playerMap) {
            totalScore += playerMap['score'] as int;
          });

          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                totalScore.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 60,
                ),
              ),
              Text(
                '${(totalScore * 0.1).toStringAsFixed(2)} EUR',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.grey,
                ),
              ),
            ],
          );
        }

        return const SizedBox();
      },
    );
  }
}
