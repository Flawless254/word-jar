import 'package:flutter/material.dart';

class TrailingCounter extends StatelessWidget {
  final int count;
  final VoidCallback? onAdd;
  final VoidCallback? onRemove;

  const TrailingCounter({
    Key? key,
    required this.count,
    this.onAdd,
    this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          onPressed: onRemove,
          icon: Icon(Icons.remove),
        ),
        Text(count.toString()),
        IconButton(
          onPressed: onAdd,
          icon: Icon(Icons.add),
        ),
      ],
    );
  }
}
